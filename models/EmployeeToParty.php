<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employee_to_party".
 *
 * @property int $id
 * @property int $employee_id
 * @property int $party_id
 */
class EmployeeToParty extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employee_to_party';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['employee_id', 'party_id'], 'required'],
            [['employee_id', 'party_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employee_id' => 'Employee ID',
            'party_id' => 'Party ID',
        ];
    }
}
