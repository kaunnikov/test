<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employee_to_skill".
 *
 * @property int $id
 * @property int $employee_id
 * @property int $skill_id
 */
class EmployeeToSkill extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employee_to_skill';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['employee_id', 'skill_id'], 'required'],
            [['employee_id', 'skill_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employee_id' => 'Employee ID',
            'skill_id' => 'Skill ID',
        ];
    }
}
