<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employee".
 *
 * @property int $id
 * @property string $name
 * @property int $inPlace
 */
class Employee extends \yii\db\ActiveRecord
{

    public $attr_parties;
    public $attr_skills;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employee';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['inPlace', 'attr_parties','attr_skills'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Фамилия',
            'inPlace' => 'На месте',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getParties()
    {
        return $this
            ->hasMany(Party::class, ['id' => 'party_id'])
            ->viaTable('employee_to_party', ['employee_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getSkills()
    {
        return $this
            ->hasMany(Skill::class, ['id' => 'skill_id'])
            ->viaTable('employee_to_skill', ['employee_id' => 'id']);
    }
}
