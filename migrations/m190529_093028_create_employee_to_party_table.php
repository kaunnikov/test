<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%employee_to_party}}`.
 */
class m190529_093028_create_employee_to_party_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        return $this->createTable('{{%employee_to_party}}', [
            'id' => $this->primaryKey(),
            'employee_id' => $this->integer()->notNull(),
            'party_id' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%employee_to_party}}');
    }
}
