<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%employee_to_skill}}`.
 */
class m190529_093019_create_employee_to_skill_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        return $this->createTable('{{%employee_to_skill}}', [
            'id' => $this->primaryKey(),
            'employee_id' => $this->integer()->notNull(),
            'skill_id' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%employee_to_skill}}');
    }
}
