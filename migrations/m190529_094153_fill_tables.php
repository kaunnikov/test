<?php

use app\models\Employee;
use app\models\Party;
use app\models\Skill;
use Faker\Factory;
use yii\db\Migration;

/**
 * Class m190529_094153_fill_tables
 */
class m190529_094153_fill_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {


        $skills = [
            [
                'name' => 'a',
            ],
            [
                'name' => 'b',
            ],
            [
                'name' => 'c',
            ],
        ];

        $parties = [
            [
                'name' => '1',
            ],
            [
                'name' => '2',
            ],
            [
                'name' => '3',
            ],
            [
                'name' => '4',
            ],
        ];

        foreach ($skills as $item) {
            $skill = new Skill();
            if ($skill->load($item, '')) {
                $skill->save();
            }
        }

        foreach ($parties as $item) {
            $party = new Party();
            if ($party->load($item, '')) {
                $party->save();
            }
        }

        for ($i = 0; $i < 6; $i++) {
            $faker = Factory::create('ru_RU');
            $name = $faker->lastName;

            $employee = new Employee();
            $employee->name = $name;
            $employee->inPlace = rand(0, 1);
            $employee->save();
        }
        return true;


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190529_094153_fill_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190529_094153_fill_tables cannot be reverted.\n";

        return false;
    }
    */
}
