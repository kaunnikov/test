<?php

use app\models\Employee;
use app\models\EmployeeToParty;
use app\models\EmployeeToSkill;
use app\models\Party;
use app\models\Skill;
use yii\db\Migration;
use yii\helpers\ArrayHelper;

/**
 * Class m190529_101345_fill_tables
 */
class m190529_101345_fill_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $employees = Employee::find()->all();

        $countSkill = Skill::find()->count();
        $countGroup = Party::find()->count();

        $sqlSkill = "select id from skill order by rand() limit :limitSkill";
        $sqlParty = "select id from party order by rand() limit :limitParty";

        foreach ($employees as $employee) {
            $employeeSkillCount = rand(1, $countSkill);
            $employeePartyCount = rand(1, $countGroup);



            $skillIds = Yii::$app->db->createCommand($sqlSkill)->bindValue(':limitSkill', $employeeSkillCount)->queryAll();
            $partyIds = Yii::$app->db->createCommand($sqlParty)->bindValue(':limitParty', $employeePartyCount)->queryAll();

            $skillIds = ArrayHelper::getColumn($skillIds, 'id');
            $partyIds = ArrayHelper::getColumn($partyIds, 'id');

            foreach ($partyIds as $id){
                $employeeToParty = new EmployeeToParty();
                $employeeToParty->employee_id = $employee->id;
                $employeeToParty->party_id = $id;
                $employeeToParty->save();
            }

            foreach ($skillIds as $id){
                $employeeToSkill = new EmployeeToSkill();
                $employeeToSkill->employee_id = $employee->id;
                $employeeToSkill->skill_id = $id;
                $employeeToSkill->save();
            }

        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190529_101345_fill_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190529_101345_fill_tables cannot be reverted.\n";

        return false;
    }
    */
}
