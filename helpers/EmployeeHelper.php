<?php

namespace app\helpers;

use app\models\Party;
use app\models\Skill;
use yii\helpers\ArrayHelper;

class EmployeeHelper
{
    public static function getInPlaceValue()
    {
        return [
            '0' => 'Нет',
            '1' => 'Да',
        ];
    }

    public static function getSkillsValue()
    {
        $data = Skill::find()->asArray()->all();
        return ArrayHelper::map($data, 'id', 'name');
    }

    public static function getPartyValue()
    {
        $data = Party::find()->asArray()->all();
        return ArrayHelper::map($data, 'id', 'name');
    }
}