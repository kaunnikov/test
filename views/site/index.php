<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmployeeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Тестовое задание Каунников';
?>
<br>
<div class="site-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'name',
            [
                'attribute' => 'attr_parties',
                'value' => function ($model) {
                    if (!$parties = $model->parties) {
                        return 'Не состоит в группах';
                    }
                    $res = [];
                    foreach ($parties as $party) {
                        $res[] = $party->name;
                    }
                    return implode(', ', $res);
                },
                'label' => 'Группы',
                'filter' => \app\helpers\EmployeeHelper::getPartyValue(),
            ],
            [
                'attribute' => 'attr_skills',
                'value' => function ($model) {
                    if (!$skills = $model->skills) {
                        return 'Нет навыков';
                    }
                    $res = [];
                    foreach ($skills as $skill) {
                        $res[] = $skill->name;
                    }
                    return implode(', ', $res);
                },
                'label' => 'Навыки',
                'filter' => \app\helpers\EmployeeHelper::getSkillsValue(),
            ],
            [
                'attribute' => 'inPlace',
                'value' => function ($model) {
                    if ($model->inPlace) {
                        return Html::tag('label', 'Да', ['class' => 'label label-success']);
                    }
                    return Html::tag('label', 'Нет', ['class' => 'label label-danger']);
                },
                'format' => 'raw',
                'filter' => \app\helpers\EmployeeHelper::getInPlaceValue(),
            ],

        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>